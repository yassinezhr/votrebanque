<%@page import="com.example.demo.entities.Client"%>
<%@ page contentType="application/pdf" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page import="net.sf.jasperreports.engine.*" %>
<%@ page import="net.sf.jasperreports.engine.data.*" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>

<%
try{

	Collection<Client> c = (Collection<Client>)request.getAttribute("listclient");
	JRDataSource jrDataSource = new JRBeanCollectionDataSource(c);
	String jrxmlFile = session.getServletContext().getRealPath("/rapports/helloreport.jrxml");
	InputStream input = new FileInputStream(new File(jrxmlFile));
	JasperReport jasperReport = JasperCompileManager.compileReport(input);
	JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, jrDataSource);
	JasperExportManager.exportReportToPdfStream(jasperPrint,response.getOutputStream());
	response.getOutputStream().flush();
	response.getOutputStream().close();
	
	
}
catch(Exception e) {
	System.out.print(e.getMessage());
	
}
%>