package com.example.demo.sec;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
@Configuration  // c'est une classe de configuration
@EnableWebSecurity //activer la sécurité web
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired
	private DataSource dataSource; // c'est notre base de données(c'est quel base de données)
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	/*
	 Memorie authentification
	auth.inMemoryAuthentication().withUser("admin").password("1234").roles("ADMIN","USER");
	auth.inMemoryAuthentication().withUser("user").password("1234").roles("USER");
	*/
	auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery("select username as principal ,password as credentials ,active from users where username=?").authoritiesByUsernameQuery("select username as principal , roles as role from users_roles where username =? ").rolePrefix("ROLE_").passwordEncoder(new Md5PasswordEncoder());
}

@Override

	protected void configure(HttpSecurity http) throws Exception {
	//http.formLogin();  si on fait ça spring sécurité va nous donner un formulaire html pour saisir username et password 
	http.formLogin().loginPage("/login"); // quand on fait ça on dit a spring quand va passer par un formaulaire quanq le fait nous meme ; passer par une authentification baser sur un formulaire
	http.authorizeRequests().antMatchers("/operations","/consultercompte").hasRole("USER");
	http.authorizeRequests().antMatchers("/saveOperation","/client","/suppC","/editC","/modifierClient","/gestioncompte").hasRole("ADMIN");
	http.exceptionHandling().accessDeniedPage("/403"); // quand il n a pas le droit acceder il va vers une action qui s'appelle /403
	}
}
