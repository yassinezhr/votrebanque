package com.example.demo.metier;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.CompteCourantRepository;
import com.example.demo.dao.CompteRepository;
import com.example.demo.dao.OperationRepository;
import com.example.demo.entities.Client;
import com.example.demo.entities.Compte;
import com.example.demo.entities.CompteCourant;
import com.example.demo.entities.CompteEpargne;
import com.example.demo.entities.Operation;
import com.example.demo.entities.Retrait;
import com.example.demo.entities.Versement;
@Service
@Transactional
public class BanqueMetierImpl implements IBanqueMetier{
	@Autowired
private CompteRepository compteRepository;
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private CompteCourantRepository compteCourantRepository;
	
	@Autowired
	private OperationRepository operationRepository;
	@Override
	public Compte consulterCompte(String codeCpte) {
		Compte cp=compteRepository.findOne(codeCpte);
		if(cp==null)throw new RuntimeException("Compte introuvable");
		return cp;
	}

	@Override
	public void verser(String codeCpte, double montant) {
		Compte cp=consulterCompte(codeCpte);
		Versement v=new Versement(new Date(), montant, cp);
		operationRepository.save(v);
	cp.setSolde(cp.getSolde()+montant);
	compteRepository.save(cp);
	}

	@Override
	public void retirer(String codeCpte, double montant) {
		Compte cp=consulterCompte(codeCpte);
		double facilitesCaisse=0;
		if(cp instanceof CompteCourant) facilitesCaisse=((CompteCourant) cp).getDecouvert();
		if(cp.getSolde()+facilitesCaisse<montant) throw new RuntimeException("Solde insuffisant");
		Retrait r=new Retrait(new Date(), montant, cp);
		operationRepository.save(r);
	cp.setSolde(cp.getSolde()-montant);
	compteRepository.save(cp);
		
	}

	@Override
	public void virement(String codeCpte1, String codeCpte2, double montant) {
		if(codeCpte1.equals(codeCpte2)){ throw new RuntimeException("Impossible virement sur le même compte");}
		retirer(codeCpte1, montant);
		verser(codeCpte2, montant);
		
	}

	@Override
	public Page<Operation> listOperation(String codeCpte, int page, int size) {
		
		return operationRepository.listOperation(codeCpte,new PageRequest(page, size));
	}

	@Override
	public void ajouterClient(String nom,String email) {
	Client 	c = new Client(nom, email);
    clientRepository.save(c);
	}

	@Override
	public void suppClient(Long idClient) {
	Client c=clientRepository.findOne(idClient);
	clientRepository.delete(c);
		
	}

	@Override
	public Client getClient(Long idClient) {
		Client client = clientRepository.findOne(idClient);
		return client;
	}

	@Override
	public void modifierClient(String nom, String email, Long code) {
		clientRepository.modClient(nom, email, code);
		
	}

	@Override
	public Collection<Client> listClient() {
		
		return clientRepository.listClient();
	}

	@Override
	public Collection<Operation> listOperationClient(String codeCpte) {
		return operationRepository.listOperationClient(codeCpte);
	}

	@Override
	public void ajouterCompteEpargne(String code, Date date, double montant, Client c, double taux) {
		CompteEpargne ce=new CompteEpargne(code, new Date(), montant, c, taux);
		compteRepository.save(ce);
		
	}

	@Override
	public void ajouterCompteCourant(String code, Date date, double montant, Client c, double decouvert) {
		CompteCourant cc=new CompteCourant(code, new Date(), montant, c, decouvert);
		compteRepository.save(cc);
		
	}

	@Override
	public Collection<Compte> listComptes() {
		Collection<Compte> listCompte = compteRepository.listComptes();
		return listCompte;
	}

	@Override
	public Collection<CompteCourant> listComptesCourant() {
		Collection<CompteCourant> listCC = compteCourantRepository.listComptesCourant();
		return listCC;
	}

	@Override
	public Collection<Client> listClientRechercher(String mc) {
		Collection<Client> listcr = clientRepository.listClientRechercher(mc);
		return listcr;
	}

	@Override
	public Page<Client> listClientRechercherPage(String mc, int page, int size) {
		return clientRepository.listClientRechercherPage(mc, new PageRequest(page, size));
	}

	

	/*@Override
	public Page<Client> listClient(Long codeClient, int page, int size) {
		
		return clientRepository.listClient(codeClient, new PageRequest(page, size));
	}*/
	
	

}
