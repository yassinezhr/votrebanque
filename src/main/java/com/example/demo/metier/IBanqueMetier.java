package com.example.demo.metier;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Client;
import com.example.demo.entities.Compte;
import com.example.demo.entities.CompteCourant;
import com.example.demo.entities.Operation;

public interface IBanqueMetier {
    public void ajouterClient(String nom,String email);
    public void suppClient(Long idClient);
    public Client getClient (Long idClient);
    public void modifierClient(String nom, String email, Long code);
    public void ajouterCompteEpargne(String code,Date date,double montant,Client c,double taux );
    public void ajouterCompteCourant(String code,Date date,double montant,Client c,double decouvert );
	public Compte consulterCompte(String codeCpte);
    public void verser(String codeCpte,double montant );
	public void retirer(String codeCpte,double montant );
	public void virement(String codeCpte1,String codeCpte2 ,double montant );
    public Page<Operation> listOperation(String codeCpte,int page,int size );
   // public Page<Client> listClient(Long codeClient,int page,int size);
    public Collection<Client> listClient();
    public Collection<Operation> listOperationClient(String codeCpte);
    public Collection<Compte> listComptes();
    public Collection<CompteCourant> listComptesCourant();
    public Collection<Client> listClientRechercher(String mc);
    public Page<Client> listClientRechercherPage(String mc,int page,int size );
}
