package com.example.demo;


import java.awt.List;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.demo.dao.ClientRepository;
import com.example.demo.dao.CompteRepository;
import com.example.demo.dao.OperationRepository;
import com.example.demo.entities.Client;
import com.example.demo.entities.Compte;
import com.example.demo.entities.CompteCourant;
import com.example.demo.entities.CompteEpargne;
import com.example.demo.entities.Operation;
import com.example.demo.entities.Retrait;
import com.example.demo.entities.Versement;
import com.example.demo.metier.IBanqueMetier;

@SpringBootApplication
public class VotreBanqueApplication implements CommandLineRunner {
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private CompteRepository compteRepository;
	
	@Autowired
	private OperationRepository operationRepository;
	
	@Autowired
	private IBanqueMetier banqueMetier;
	
	public static void main(String[] args) {
		
		
		SpringApplication.run(VotreBanqueApplication.class, args);
	
	}

	@Override
	public void run(String... arg0) throws Exception {
		
		/*
		
		Client c1= clientRepository.save(new Client("Yassine", "Yassine@gmail.com"));
		Client c2= clientRepository.save(new Client("Amine", "Amine@gmail.com"));
		Client c3= clientRepository.save(new Client("Badr", "Badr@gmail.com"));
		
		
		Compte cp1 = compteRepository.save(new CompteCourant("cp1", new Date(), 41278, c1, 47841));
		Compte cp2 = compteRepository.save(new CompteEpargne("cp2", new Date(), 20000, c2, 5.5));
		
		operationRepository.save(new Versement(new Date(), 200, cp1));
		operationRepository.save(new Versement(new Date(), 300, cp1));
		operationRepository.save(new Versement(new Date(), 400, cp1));
		operationRepository.save(new Retrait(new Date(), 100, cp1));
		
		operationRepository.save(new Versement(new Date(), 200, cp2));
		operationRepository.save(new Versement(new Date(), 300, cp2));
		operationRepository.save(new Versement(new Date(), 400, cp2));
		operationRepository.save(new Retrait(new Date(), 100, cp2));
		
		
		banqueMetier.verser("cp1", 10000);
		
		
		banqueMetier.ajouterClient("Jawad", "jawad@gmail.com");
	//ca marche 	banqueMetier.suppClient((long) 1);// il va supprimer yassine son id est 1
	// ca marche 	banqueMetier.modifierClient("YASSINE", "yassine@gmail.com", (long)3);
		
		
		Collection<Client> CollectionClient=banqueMetier.listClient();
		for (Client client : CollectionClient) {
			System.out.println(client.getCode()+"/"+client.getNom() +"/"+client.getEmail());
		}
		
		Collection<Operation> CollectionOperationClient=banqueMetier.listOperationClient("cp2");
		for (Operation operation : CollectionOperationClient) {
			System.out.println(operation.getNumero()+"/"+operation.getCompte().getCode() +"/"+operation.getCompte().getClient().getNom());
		}
		
		*/
		
		
	}
}
