package com.example.demo.web;

import java.util.Collection;
import java.util.Date;

import javax.print.attribute.standard.PagesPerMinute;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dao.ClientRepository;
import com.example.demo.entities.Client;
import com.example.demo.entities.Compte;
import com.example.demo.entities.CompteCourant;
import com.example.demo.entities.Operation;
import com.example.demo.metier.IBanqueMetier;

@Controller
public class BanqueController {
	@Autowired
	private IBanqueMetier banqueMetier;
	
	@RequestMapping("/operations")  // c'est une action
	public String index(){
	return "comptes" ;
	}
	
	@RequestMapping("/index")
	public String raport(Model model){
		Collection<Client> listclient = banqueMetier.listClient();
		model.addAttribute("listclient", listclient);
		
	return "index.jsp" ;
	}
	
	@RequestMapping("/gestioncompte")
	public String gestioncomptes(Model model){
		Collection<Compte> listCompte = banqueMetier.listComptes();
		Collection<CompteCourant> listCC=banqueMetier.listComptesCourant();
		model.addAttribute("listCC", listCC);
		model.addAttribute("listCompte", listCompte);
	return "gestioncomptes" ;
	}
	
	
	@RequestMapping("/consultercompte")
	public String consulter(Model model,String codeCompte,@RequestParam(name="page",defaultValue="0")int page,@RequestParam(name="size",defaultValue="5")int size){
		model.addAttribute("CodeCompte", codeCompte);
		try {
			Compte cp=banqueMetier.consulterCompte(codeCompte);// s'il n y a pas un compte on va avoir une exception
			Page<Operation> pageOperations = banqueMetier.listOperation(codeCompte, page, size);
			model.addAttribute("listOperations", pageOperations.getContent());
			int[] pages = new int[pageOperations.getTotalPages()];
			model.addAttribute("pages", pages);
			model.addAttribute("compte", cp);
			
		} catch (Exception e) {
			model.addAttribute("exception", e);
		}
		
	return "comptes" ;
	}
	
	@RequestMapping("/client")
	public String ajouterClient(Model model,@RequestParam(name="mc",defaultValue="")String mc){
		Collection<Client> listC =banqueMetier.listClientRechercher("%"+mc+"%");
		model.addAttribute("listC", listC);
		model.addAttribute("mc",mc);
	return "ajouterClient" ;
	}
	
	@RequestMapping(value="/saveClient",method=RequestMethod.POST)
	public String saveClient(Model model,String nom,String email){
		try {
			banqueMetier.ajouterClient(nom, email);
		} catch (Exception e) {
			model.addAttribute("error", e);
			
		}
		return "redirect:/client" ; // car on va se dériger à une action (/client)
	}
	
	
	@RequestMapping("/suppC")
	public String suppC(Long code){
		banqueMetier.suppClient(code);
	return "redirect:/client" ;
	}
	
	@RequestMapping("/editC")
	public String editC(Model model,Long code){
		Client client=banqueMetier.getClient(code);
		model.addAttribute("client", client);
	return "editclient" ;
	}
	
	@RequestMapping("/modifierClient")
	public String modifierC(Long code,String nom,String email){
		banqueMetier.modifierClient(nom, email, code);
		
	return "redirect:/client" ;
	}
	
	@RequestMapping("/clientpage")
	public String Clientpage(Model model,@RequestParam(name="mc",defaultValue="")String mc,@RequestParam(name="page",defaultValue="0")int page,@RequestParam(name="size",defaultValue="3")int size){
		Page<Client> clientpage = banqueMetier.listClientRechercherPage("%"+mc+"%", page, size);
		model.addAttribute("clientpage", clientpage.getContent());
		int[] pages = new int[clientpage.getTotalPages()];
		model.addAttribute("pages", pages);
		model.addAttribute("mc",mc);
	return "Clientpage" ;
	}
	
	
	@RequestMapping(value="/saveOperation",method=RequestMethod.POST)
	public String saveOperation(Model model,String typeOperation,String codeCompte,double montant,String codeCompte2){
		try {
			if (typeOperation.equals("VERS")){
				banqueMetier.verser(codeCompte, montant);
			}
			else if (typeOperation.equals("RETR")){
				banqueMetier.retirer(codeCompte, montant);
			}
			else if (typeOperation.equals("VIR")){
				banqueMetier.virement(codeCompte, codeCompte2, montant);
			}
		} catch (Exception e) {
			model.addAttribute("error", e);
			return "redirect:/consultercompte?codeCompte="+codeCompte+"&error="+e.getMessage() ;
		}
		return "redirect:/consultercompte?codeCompte="+codeCompte ;
	}
	
	@RequestMapping(value="/saveCompte",method=RequestMethod.POST)
	public String saveCompte(Model model,String codeCompte,double solde,Long codeclient,String typeCompte,Long decouvert,Long taux){
		try {
			if (typeCompte.equals("CE")){
				Client c =banqueMetier.getClient(codeclient);
				banqueMetier.ajouterCompteEpargne(codeCompte,new Date(), solde, c, taux);
				
			}
			else if (typeCompte.equals("CC")){
				Client c =banqueMetier.getClient(codeclient);
				banqueMetier.ajouterCompteCourant(codeCompte, new Date(), solde, c, decouvert);
			}
	
		} catch (Exception e) {
			model.addAttribute("error", e);
			
		}
		return "redirect:/gestioncompte" ;
	}

}
