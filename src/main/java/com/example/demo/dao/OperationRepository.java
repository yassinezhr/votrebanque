package com.example.demo.dao;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Client;
import com.example.demo.entities.Operation;

public interface OperationRepository extends JpaRepository<Operation, Long> {
@Query("select o from Operation o where o.compte.code=:x order by o.dateOperation desc ")
	public Page<Operation> listOperation(@Param("x")String codeCpte, Pageable pageable);

@Query("select o from Operation o where o.compte.code=:x order by o.dateOperation desc ")
public Collection<Operation> listOperationClient(@Param("x")String codeCpte);
}
