package com.example.demo.dao;

import java.awt.List;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Client;
import com.example.demo.entities.Operation;

public interface ClientRepository extends JpaRepository<Client, Long> {
	// pour l'ajout on la méthode save ; clientRepository.save(c);
		//pour la suppression on a la méthode delete ; clientRepository.delete(c);
		/*pour récupérer on a la méthode findone ; 
		   -Client c=clientRepository.findOne(idClient);
		   */
		//pour la modification et select une list on doit les faire 
	
	//les deux marche bien
	
	/*
	@Modifying
	@Query("update Client c set c.nom = ?1, c.email = ?2 where c.code = ?3")
	void modClient(String nom, String email, Long code);
	
	*/

	
	
	@Query("update Client c set c.nom = :nom , c.email= :email  where c.code = :code")
	@Modifying
	public void modClient(@Param("nom")String nom,@Param("email")String email, @Param("code") Long code);
	
	
	/*@Query("select c from Client c where c.code=:x ")
	public Page<Client> listClient(@Param("x")Long codeClient, Pageable pageable);*/
	
	@Query("select c from Client c  ")
	public Collection<Client> listClient();
	
	
	@Query("select c from Client c where c.nom like :nom ")
	public Collection<Client> listClientRechercher(@Param("nom")String mc);
	
	@Query("select c from Client c where c.nom like :nom ")
	public Page<Client> listClientRechercherPage(@Param("nom")String mc,Pageable pageable);
	
	
	
}
