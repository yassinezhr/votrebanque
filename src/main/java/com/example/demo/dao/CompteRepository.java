package com.example.demo.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entities.Compte;

public interface CompteRepository extends JpaRepository<Compte, String> {
	@Query("select c from Compte c  ")
	public Collection<Compte> listComptes();
	
}
