package com.example.demo.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.entities.CompteCourant;

public interface CompteCourantRepository extends JpaRepository<CompteCourant, String>  {
	@Query("select c from CompteCourant c  ")
	public Collection<CompteCourant> listComptesCourant();
	
}
